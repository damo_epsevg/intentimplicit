package edu.upc.damo;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends Activity {

    EditText lat, lon;
    EditText numeroTel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        inicialitza();
    }

    private void inicialitza() {
        lat = (EditText) findViewById(R.id.lat);
        lon = (EditText) findViewById(R.id.lon);
        numeroTel = (EditText) findViewById(R.id.tel);

        Button b;

        b = (Button) findViewById(R.id.gmaps);
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mostraPunt(v);
            }
        });

        b = (Button) findViewById(R.id.web);
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mostraWeb(v);
            }
        });

        b = (Button) findViewById(R.id.marcarTel);
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                marcarTel(v);
            }
        });
    }

    private void marcarTel(View v){
        String valorTel = numeroTel.getText().toString();

        if (valorTel.equals("")) return;

        Uri uri = Uri.parse("tel:"+valorTel);
        Intent i = new Intent(Intent.ACTION_DIAL, uri);


        startActivity(i);
    }


    private void mostraWeb(View v) {
        Uri uri = Uri.parse("http://commonsware.com");
        Intent i = new Intent(Intent.ACTION_VIEW, uri);
        startActivity(i);
    }


    private void mostraPunt(View v) {
        String valorLat = lat.getText().toString();
        String valorLon = lon.getText().toString();
//        Uri uri = Uri.parse("geo:" + valorLat + "," + valorLon + "?q=" + valorLat + "," + valorLon);
        Uri uri = Uri.parse("geo:"+valorLat+","+valorLon+"?q="+valorLat+","+valorLon+"(Aquí");
        Toast.makeText(this,"NO xuta en l'emulador",Toast.LENGTH_LONG);
         Intent i = new Intent(Intent.ACTION_VIEW, uri);
        startActivity(i);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar iteprivate  clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }



}
